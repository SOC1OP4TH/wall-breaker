# Wall Breaker
This is a 2D game where you must avoid the walls and reach the finish line. You have 3 hearts. If you collide with a wall, you will lose 1 heart. If you lose all of your hearts, the game is over.

## Controls

* Use the arrow keys to move the player.
## Objective

The objective of the game is to reach the finish line without losing all of your hearts.

## Tips

* Be careful when moving around the walls.
* Try to memorize the map so that you can avoid the walls more easily.
* If you are low on hearts, be extra careful and try to avoid the walls at all costs.
## How to Play

To play the game, simply press the Play button. The game will start and you will be able to move the player around the screen using the arrow keys. Try to reach the finish line without losing all of your hearts.

## Game Over

The game is over if you lose all of your hearts. If you lose, you can click the Restart button to try again.

### Have fun!

I hope you enjoy playing Wall Breaker!
