using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        move();
    }
    void move()
    {
       float moveHorizontal =  Input.GetAxis("Horizontal");
       float moveVertical = Input.GetAxis("Vertical");
       Vector3 force  = new Vector3(moveHorizontal,0,moveVertical);
       rb.AddForce(force*50*Time.deltaTime);
       
    }
}
