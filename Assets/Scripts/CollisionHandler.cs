using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    public Text heals;
    public Text showTimer;
    public Text gameInfo;
    public Button restart;
    Rigidbody rb;
    bool isRunning = true;
    float timer = 20;

    int intTimer;
    int heal = 3;
void Start(){
    heals.text = heal + " Hearts";
    rb = GetComponent<Rigidbody>();
    gameInfo.enabled = false;
    restart.gameObject.SetActive(false);
}
void FixedUpdate()
{
    if(isRunning)
    {
    timer-= Time.deltaTime;
    intTimer = (int)timer;
    showTimer.text = intTimer+"";
    }
    if(intTimer==0){
        isRunning = false;
         Finish();
    }
   
}
void OnCollisionEnter(Collision other)
{
    if(other.gameObject.tag == "Wall" && heal > 0)
    {
        heal--;
        heals.text = heal + " Hearts";
        if(heal==0){
            Finish();
        }
        
     
    }
    if(other.gameObject.tag=="Finish"){
        isRunning=false;
        gameInfo.text = "Congrats";
        Finish();

    }
    
}
void Finish()
{    
         GetComponent<Movement>().enabled = false; 
         rb.velocity = Vector3.zero;  
         rb.angularDrag = 0;
         gameInfo.enabled = true;
         restart.gameObject.SetActive(true);
}
public void Restart(){

    SceneManager.LoadScene("Game");
}   
}
